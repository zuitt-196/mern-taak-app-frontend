import React,{useState, useEffect} from 'react'
import TaskForm from "./TaskForm";
import Task from './Task';
import axios from 'axios';
import {toast} from 'react-toastify'
import { URL } from '../App';
import loadingIMG from '../assests/loader.gif'
import { get } from 'mongoose';

const TaskLists = () => {
    //Define task as empty array 
    const [task, setTaks] = useState([])
    const [completedtask, setCompletedtask] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    
    const [isEditing, setIsEditing] = useState(false)
    const [taskId, seTaskId] = useState("")

    //
    const [formdata, setFormDta] = useState({
            name: "Task",
            completed: false,
            
    })

    const {name} = formdata


    
    const  handleInputChange = (e) => { 
    //  const {name, value} = e.target
    //  setFormDta({...formdata, name: value})
     const {name, value} = e.target
     setFormDta({...formdata, [name]: value})

    }

//GET THE TAKS OR DATA FROM DATABASE
const getTask =  async () => {
    setIsLoading(true)
    try {
       const {data} =  await axios.get(`${URL}/api/tasks`)
    //    console.log(dataz
        setTaks(data)
       setIsLoading(false)
    } catch (error) {
        setIsLoading(false)
    }
}


useEffect(() => {
    getTask()
}, [])


   const createTask  = async (e) => {
        e.preventDefault()
        // console.log(formdata);
    if (name === "") {
        return toast.error("Input field cannot be empty");


    }

    try {
        await axios.post(`${URL}/api/tasks`, formdata);
        // to clear the data
        setFormDta({...formdata, name:""});
        toast.success("Task added successfully")
        getTask()
    } catch (error) {
        toast.error(error.message);
        console.log(error);
    }

   } 
   const deleteTask = async(id) => {
        try {
            await axios.delete(`${URL}/api/tasks/${id}`)
            getTask()
        } catch (error) {
            toast.error(error.message);
        }
    }

    useEffect(() => {
        const ctask = task.filter((x) => {
                return x.completed === true
        })
        setCompletedtask(ctask)
    }, [task])

    const getSingleTask = async (task) => {
        setFormDta({name: task.name, completed:false })
        seTaskId(task._id)
        setIsEditing(true)
    } 

    const updateTask = async (e) => {
            e.preventDefault()

           if (name === "") {
                return toast.error("Input field canniot be empty")
           } 

           try {
                await axios.put(`${URL}/api/tasks/${taskId}`, formdata)
                setFormDta({...formdata, name: ""})
                setIsEditing(false)
                getTask()
           } catch (error) {
            toast.error(error.message);
            
           }
        }

const setToComplete = async (task) => {
    const newFormData = {
        name: task.name,
        completed: true
    }

    try {
        await axios.put(`${URL}/api/tasks/${task._id}`, newFormData)    
        getTask()
    } catch (error) {
        toast.error(error.message);
            
        
    }
}

  return (
    <div>
        <h2>Taks Manager</h2>
        <TaskForm name={name} handleInputChange={handleInputChange} createTask={createTask} isEditing={isEditing} updateTask={updateTask}/>
        {task.length > 0 && (
             <div className='--flex-between --pb'>
             <p>
                 <b>Total Taks:</b> {task.length}
             </p>
             <p>
                 <b>Completed Taks:</b> {completedtask.length}
             </p>
     </div>
        )}
        <hr />
        
        {   
            isLoading && (
                <div className="--flex-center">
                    <img src={loadingIMG} alt="loading" />
                </div>
            )
        }

        {
            !isLoading && task.length === 0 ? (
                <p className='--py'>No task added. Plss adde a task</p>
            ) : (
                <>
                    {task.map((task, index) =>{
                        return(
                            <Task key={index} task={task} index={index} deleteTask={deleteTask} getSingleTask={getSingleTask}  setToComplete={ setToComplete}/>
                        )
                    })}

                </>
            )
        }

     
    </div>
  )
}

export default TaskLists